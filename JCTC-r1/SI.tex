\documentclass[a4paper,11pt]{article}
\usepackage{xspace}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[perpage,para,symbol]{footmisc}
\usepackage{fancyhdr}
\usepackage{floatrow}
\setlength{\oddsidemargin}{-5mm}
\setlength{\textwidth}{168mm}
\setlength{\topmargin}{-10mm}
\setlength{\headsep}{10mm}
\setlength{\textheight}{240mm}

\renewcommand{\thefootnote}{\alph{footnote}}
\renewcommand\thesection{\sffamily\Alph{section}}
\newcommand\cpp{C\raisebox{1pt}{++}}
\newcommand{\va}{\mathrm{\bf{A}}}
\newcommand{\vb}{\mathrm{\bf{B}}}
\newcommand{\vc}{\mathrm{\bf{c}}}
\newcommand{\vp}{\mathrm{\bf{p}}}
\newcommand{\vr}{\mathrm{\bf{r}}}
\newcommand{\vx}{\mathrm{\bf{x}}}
\newcommand{\mi}{\mathrm{i}}
\newcommand{\Det}{\mathrm{Det}}
\newcommand{\diff}{\mathrm{d}}

\newcommand{\cunh}[0]{[\text{Cu(NH}_3)_4]^{2+}}
\newcommand{\cuwater}[0]{[\text{Cu(H}_2\text{O})_4]^{2+}}
\newcommand{\cucl}[0]{[\text{CuCl}_4]^{2-}}
\newcommand{\ci}[0]{c_{I}^k}
\newcommand{\Space}[0]{\mathcal{S}}
\newcommand{\psiref}[0]{\Psi_k^{(0)}}
\newcommand{\eref}[0]{E_k^{(0)}}
\newcommand{\ket}[1]{{\ensuremath{|#1\rangle}\xspace}}
\newcommand{\bra}[1]{{\ensuremath{\langle #1|}\xspace}}
\newcommand{\dethf}[0]{{\ensuremath{\ket{i\bar{i}a}}\xspace}}
\newcommand{\excita}[0]{{\ensuremath{\hat{\tau}_{iv}^{\alpha}}\xspace}}
\newcommand{\excitb}[0]{{\ensuremath{\hat{\tau}_{iv}^{\beta}}\xspace}}
\newcommand{\singleonea}[0]{{\ensuremath{\hat{\tau}_{iv}^{\alpha}\ket{\rm I}}\xspace}}
\newcommand{\singleoneb}[0]{{\ensuremath{\hat{\tau}_{iv}^{\beta}\ket{\rm I}}\xspace}}
\newcommand{\singletwoa}[0]{{\ensuremath{\hat{\tau}_{iv}^{\alpha}\ket{\rm II}}\xspace}}
\newcommand{\singletwob}[0]{{\ensuremath{\hat{\tau}_{iv}^{\beta}\ket{\rm II}}\xspace}}
\newcommand{\detone}[0]{{\ensuremath{\ket{\rm I}\xspace}}}
\newcommand{\dettwo}[0]{{\ensuremath{\ket{\rm II}\xspace}}}
\newcommand{\rohf}[0]{{\ensuremath{\ket{\rm ROHF}\xspace \,}}}
\newcommand{\lmct}[0]{{\ensuremath{\ket{\rm LMCT}\xspace \,}}}
\newcommand{\coefannex}[0]{{\ensuremath{\lambda}\xspace}}
\newcommand{\detct}[0]{{\ensuremath{\ket{i\bar{a}a}}\xspace}}
\newcommand{\lethf}[0]{{\ensuremath{\bra{i\bar{i}a}}\xspace}}
\newcommand{\letct}[0]{{\ensuremath{\bra{i\bar{a}a}}\xspace}}
\newcommand{\detdouble}[0]{{\ensuremath{\ket{i\bar{a}r}}\xspace}}
\newcommand{\pdethf}[0]{{\ensuremath{\ket{i'\bar{i'}a'}}\xspace}}
\newcommand{\pdetct}[0]{{\ensuremath{\ket{i'\bar{a'}a'}}\xspace}}
\newcommand{\pdetdouble}[0]{{\ensuremath{\ket{i'\bar{a'}r}}\xspace}}
\newcommand{\ldethf}[0]{{\ensuremath{\bra{i'\bar{i'}a'}}\xspace}}
\newcommand{\ldetct}[0]{{\ensuremath{\bra{i'\bar{a'}a'}}\xspace}}
\newcommand{\ldetdouble}[0]{{\ensuremath{\bra{i'\bar{a'}r}}\xspace}}
\newcommand{\ohop}[0]{\ensuremath{|({\rm 1h}_i{\rm 1p}_{r})^{\sigma}\rangle\xspace}}
\newcommand{\ohopalpha}[0]{\ensuremath{|({\rm 1h_i}{\rm 1p}_{r})^{\alpha}\rangle\xspace}}
\newcommand{\ohopbeta}[0]{\ensuremath{|({\rm 1h}_i{\rm 1p}_{r})^{\beta}\rangle\xspace}}
\newcommand{\oh}[0]{\ensuremath{|{\rm 1h}_{ia}\rangle}}
\newcommand{\op}[0]{\ensuremath{|{\rm 1p}_{ar}\rangle}}
\newcommand{\elemm}[3]{{\ensuremath{\bra{#1}{#2}\ket{#3}}\xspace}}
\newcommand{\ovrlp}[2]{{\ensuremath{\langle #1|#2\rangle}\xspace}}
\newcommand{\kapa}[0]{\ket{\mu}}
\newcommand{\kair}[0]{\langle i | K_{a} | v \rangle}
\newcommand{\kbir}[0]{\langle i | K_{b} | v \rangle}


\renewcommand\refname{}

\title{Supporting information}
\begin{document}

\sffamily
\section*{\sffamily Interplay between electronic correlation and metal-ligand delocalization in the spectroscopy of transition metal compounds: case study on a series of planar Cu$^{2+}$ complexes.} 

\section{ROHF and LMCT wave function in strongly localized orbitals}
The ROHF SOMO $S$ and ligand donor orbital $L$ are quite localized but a small amount of metal-ligand delocalization occurs as the spin density on the copper atom is slightly smaller than one. Nevertheless, by rotating $S$ and $L$ one can obtain strongly localized orbitals, namely $s$ and $l$, which are generally defined as:
\begin{equation}
 \label{eq:loc_orb}
 \begin{aligned}
  S & = cos(\theta) \,\, s - sin(\theta) \,\,l \\
  L & = cos(\theta) \,\, l + sin(\theta) \,\,s
 \end{aligned}
\end{equation}
with $\theta \approx 0$ as the metal-ligand covalency is quite small at the ROHF level.  
There are several way of obtaining $s$ and $l$, one could be for example to minimize the amount of ligand atomic orbital component in $s$. 

Once obtained the localized orbitals, one can rewrite the ROHF wave function in terms of $l$ and $s$. 
The ROHF wave function is written as: 
\begin{equation}
 \label{eq:rohf}
  \ket{\text{ROHF}} = \ket{c\bar{c}\,i\bar{i}\,L\bar{L}\,S}
\end{equation}
where $c\bar{c}$ denotes an arbitrary number of doubly occupied orbitals and $i$ a doubly occupied orbital.  
Inserting \eqref{eq:loc_orb} is \eqref{eq:rohf} gives: 
\begin{equation}
 \ket{\text{ROHF}} =  cos(\theta) \,\, \ket{c\bar{c}\,i\bar{i}\,l\bar{l}\,s} + sin(\theta) \,\,\ket{c\bar{c}\,i\bar{i}\,l\bar{s}\,s} 
\end{equation}
Written with $l$ and $s$, the ROHF wave function is a linear combination of two Slater determinants with a clear VB meaning: it is dominated by $ \ket{c\bar{c}\,i\bar{i}\,l\bar{l}\,s}$ which represents a Cu$^{2+}$ oxidation state, and a small component of $\ket{c\bar{c}\,i\bar{i}\,l\bar{s}\,s}$ is present which represents a Cu$^+$ oxidation state. Consequently, the ROHF wave function contains already the resonance of two copper oxidation states. 

The LMCT wave function is written in strongly localized orbitals as:
\begin{equation}
 \begin{aligned}
  \ket{LMCT} & = a^{\dagger}_{\bar{S}} \, a_{\bar{L}} \, \ket{\text{ROHF}} \\
             & =  cos(\theta) \,\, \ket{c\bar{c}\,i\bar{i}\,l\bar{s}\,s} - sin(\theta) \,\,\ket{c\bar{c}\,i\bar{i}\,l\bar{l}\,s} 
 \end{aligned}
\end{equation}
Therefore the LMCT Slater determinant is a linear combination of the same two Slater determinant, but it represents a state dominated by a Cu$^+$ oxidation state. 

\section{The second-order contribution to the energy using strongly localized orbitals}
%As it was shown in the main text that the determinants $ \ket{_{\bar{L}} ^{\bar{S}}\, _{i} ^{a}} $ play a crucial role in the metal-ligand covalency and the ligand-field splitting, it is interesting to rewrite them in term of local orbitals. Here we assume that $i$ is one of the doubly occupied $3d$ orbital and $a$ a virtual  $4d$-like orbital. By inserting the definition of \eqref{eq:loc_orb} in the definition of $ \ket{_{\bar{L}} ^{\bar{S}}\, _{i} ^{a}} $, it comes:  
%\begin{equation}
% \begin{aligned}
%  \ket{_{\bar{L}} ^{\bar{S}}\, _{i} ^{a}} & = a^{\dagger}_{a}  \,a_{i}  \,\ket{\text{LMCT}} \\
%                                          & = cos(\theta) \,\, \ket{c\bar{c}\,a\bar{i}\,l\bar{s}\,s} - sin(\theta) \,\,\ket{c\bar{c}\,a\bar{i}\,l\bar{l}\,s}
% \end{aligned}
%\end{equation}
%Such a Slater determinant is dominated by $\ket{c\bar{c}\,a\bar{i}\,l\bar{s}\,s}$ which is a single excitation $(i\rightarrow a) $ on top of the Cu$^+$ oxidation state or a double excitation $(i\rightarrow a)(\bar{L}\rightarrow \bar{S})$ on top of the Cu$^{2+}$ oxidation state. Also, $\ket{_{\bar{L}} ^{\bar{S}}\, _{i} ^{a}} $ contains a component on $\ket{c\bar{c}\,a\bar{i}\,l\bar{l}\,s}$ which is a single excitation  $(i\rightarrow a) $ on top of the Cu$^{2+}$ oxidation state  or a double excitation $(i\rightarrow a)(\bar{S}\rightarrow \bar{L})$ on top of the Cu$^{2+}$ oxidation state. 
%
As the most important differential effect appearing at second order comes from the contribution of the $\ket{_L ^{S}\, _{i,\sigma} ^{a,\sigma}} $ Slater determinants, we can analyze that effect using localized orbitals. 
Looking at the Hamiltonian matrix element $\elemm{_{\bar{L}} ^{\bar{S}}\, _{i} ^{a}}{H}{\text{ROHF}}$ it comes:
\begin{equation}
 \elemm{_{\bar{L}} ^{\bar{S}}\, _{i} ^{a}}{H}{\text{ROHF}} = (LS|ia)
\end{equation}
Such an integral can be rewritten in terms of localized orbitals, and developing $(LS|ia)$ up to first-order in $ sin(\theta)$ gives:
\begin{equation}
 \label{eq:hmat_loc}
 (LS|ia) = cos(\theta) \bigg( (ls|ia) \times cos(\theta) + \left(\,(ss|ia) - (ll|ia)\,\right)  \times  sin(\theta) \bigg)
\end{equation}
Recalling that the orbitals $i$, $a$, and $s$ are located on the copper atom and that $l$ is located on the ligand, one can establish the order of magnitude of the different integrals: 
\begin{equation} 
 \left| (ls|ia) \right| \ll \left|(ll|ia)\right| \ll \left|(ss|ia)\right|
\end{equation} 
Also, one can assign a physical meaning to each integral: $(ls|ia)$ is typical from a dispersive correlation effect as it is the interaction of two dipoles, whether $(ll|ia)$ and $(ss|ia)$ represent a polarization effect as they are the interaction of the dipole $ia$ with the charge distributions $l^2$ and $s^2$. Therefore the integral $(LS|ia)$ represents a competition between a dispersive correlation effect and a breathing orbital effect. 
Looking now at the second order contribution of the $\ket{_L ^{S}\, _{i,\sigma} ^{a,\sigma}} $ Slater determinants: 
\begin{equation}
 \begin{aligned}
  e^{(2)} & \approx \sum_{i,a} \frac{(SL|ia)^2}{\epsilon_L - \epsilon_S+ \epsilon_i  - \epsilon_a}, 
 \end{aligned}
\end{equation}
One can rewrite such an expression in terms of localized orbitals. 
\begin{equation}
 \begin{aligned}
  e^{(2)} & \approx cos^2(\theta) \sum_{i,a} \frac{ \bigg( (ls|ia) \times cos(\theta) + \left(\,(ss|ia) - (ll|ia)\,\right)  \times  sin(\theta) \bigg)^2}{\epsilon_L - \epsilon_S+ \epsilon_i  - \epsilon_a} \\
          & \approx cos^2(\theta) \sum_{i,a} \frac{ (ls|ia) \times cos^2 (\theta) + \left(\,(ss|ia) - (ll|ia)\,\right)^2 sin(\theta)^2 - 2 sin(\theta) cos(\theta) (ls|ia) * \left(\,(ss|ia) - (ll|ia)\,\right) }{\epsilon_L - \epsilon_S+ \epsilon_i  - \epsilon_a}
 \end{aligned}
\end{equation}

Therefore, the second-order correlation energy contains already a breathing orbital correlation term which is a growing function of the mixing between the metal and ligand orbitals. 
\end{document}

